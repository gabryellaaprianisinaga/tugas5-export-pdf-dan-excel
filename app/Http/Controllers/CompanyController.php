<?php

namespace App\Http\Controllers;
use App\Models\Company;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function showDataCompany() {
        $data = Company::all();
        return view('data-company', compact('data'));
    }

    public function tambahData() {
        return view('tambah-data-company');
    }

    public function simpanData(Request $request) {
        $data = new Company;
        $data->id_company = $request->input('id');
        $data->nama = $request->input('nama');
        $data->alamat = $request->input('alamat');
        $data->save();
        return redirect('data-company');
    }

    public function hapusData($slug) {
        $data = Company::find($slug);
        $data->delete();
        return redirect('data-company');
    }

    public function editData($slug) {
        $data = Company::find($slug);
        return view('edit-data-company', compact('data'));
    }

    public function simpanEditData(Request $request, $slug) {
        $data = Company::find($slug);
        $data->id_company = $request->input('id');
        $data->nama = $request->input('nama');
        $data->alamat = $request->input('alamat');
        $data->update();
        return redirect('data-company');
    }
}
