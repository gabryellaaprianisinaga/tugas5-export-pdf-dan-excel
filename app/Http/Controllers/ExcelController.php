<?php

namespace App\Http\Controllers;

use App\Exports\DataexcExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function fileExport()
    {
        return Excel::download(new DataexcExport, 'Data_Karyawan.xlsx');
    }
}
