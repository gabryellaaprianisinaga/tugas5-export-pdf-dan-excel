<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  <title>Data Karyawan</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid d-flex justify-content-center align-items-center p-2 pt-3">
        <h5 class="text-white fw-bold">DATA COMPANY</h5>
    </div>
</nav>
  <div class="container mt-4">
    <div class="mt-5">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">id</th>
            <th scope="col">nama</th>
            <th scope="col">posisi</th>
            <th scope="col">perusahaan</th>
            <th scope="col">action</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $singleData)
          <tr>
            <th scope="row">{{ $singleData->id_emp }}</th>
            <td>{{ $singleData->nama }}</td>
            <td>{{ $singleData->posisi }}</td>
            <td>{{ $singleData->perusahaan }}</td>
            <td>
              <a href="/edit/{{ $singleData->id }}" class="btn btn-warning">Edit</a>
              <a href="/hapus/{{ $singleData->id }}"class="btn btn-danger">Delete</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div class="mt-5">
        <a href="/tambah" class="btn btn-primary right">Tambah</a>
      </div>
        <div class="mt-5 mb-5">
            <a href="/home" class="btn btn-primary">Kembali</a>
        </div>
    </div>
  </div>

  <!-- Bootstrap JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
