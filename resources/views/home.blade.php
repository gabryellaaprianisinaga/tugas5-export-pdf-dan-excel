<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  <title>Home</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid d-flex justify-content-center align-items-center p-2 pt-3">
        <h5 class="text-white fw-bold">HOMEPAGE</h5>
    </div>
</nav>
  <div class="container">

    <div class="mt-5 d-flex justify-content-center me-5">
        <div class="mb-4 text-center me-5">
            <a href="/data-pekerja" class="btn btn-primary">Data Pekerja</a>
        </div>
        <div class="mb-4 text-center me-5">
            <a href="/data-company" class="btn btn-primary">Data Perusahaan</a>
        </div>
        <div class="mb-4 text-center me-5">
            <a href="/generate-pdf" class="btn btn-primary">Download data (PDF)</a>
        </div>
        <div class="text-center me-5">
            <a href="/file-export" class="btn btn-primary">Download data (Excel)</a>
        </div>
    </div>
  </div>

  <!-- Bootstrap JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

</body>
</html>
