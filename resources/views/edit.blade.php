<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  <title>Edit Data</title>
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid d-flex justify-content-center align-items-center p-2 pt-3">
        <h5 class="text-white fw-bold">EDIT PEGAWAI</h5>
    </div>
</nav>
  <div class="container">
    <div class="w-25 mt-5">
      <form action="{{ url('edit/' . $data->id) }}" method="post">
        @csrf
        <div class="mb-3">
          <label for="id-emp" class="form-label">Id</label>
          <input type="text" class="form-control" name="id-emp" id="id-emp"
          value="{{ $data->id_emp }}">
        </div>
        <div class="mb-3">
          <label for="nama" class="form-label">Nama</label>
          <input type="text" class="form-control" name="nama" id="nama"
          value="{{ $data->nama }}">
        </div>
        <div class="mb-3">
          <label for="posisi" class="form-label">Posisi</label>
          <input type="text" class="form-control" name="posisi" id="posisi" value="{{ $data->posisi }}">
        </div>
        <div class="mb-3">
          <label for="perusahaan" class="form-label">Perusahan</label>
          <input type="text" class="form-control" name="perusahaan" id="perusahaan" value="{{ $data->perusahaan }}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>

  <!-- Bootstrap JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
